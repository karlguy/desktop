# Copyright 2013 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

export_exlib_phases src_prepare

if ever is_scm; then
    SCM_REPOSITORY="https://bitbucket.org/portix/dwb.git"
    require scm-git
else
    DOWNLOADS="https://bitbucket.org/portix/dwb/downloads/${PNV}.tar.gz"
fi

SUMMARY="A lightweight web browser that intends to be mostly keyboard driven"
HOMEPAGE="http://portix.bitbucket.org/dwb/"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-libs/glib:2
        dev-libs/gnutls
        dev-libs/json-c
        gnome-desktop/libsoup:2.4
        net-libs/webkit:3.0
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3
        x11-libs/libX11
        x11-libs/pango
"

BUGS_TO="mixi@shadowice.org"

MY_MAKE_PARAMS=(
    GTK=3
    CROSS_COMPILE=$(exhost --tool-prefix)
    PREFIX=/usr/$(exhost --target)
    DATAROOTDIR=/usr/share
)
DEFAULT_SRC_COMPILE_PARAMS=( "${MY_MAKE_PARAMS[@]}" )
DEFAULT_SRC_TEST_PARAMS=( "${MY_MAKE_PARAMS[@]}" )
DEFAULT_SRC_INSTALL_PARAMS=( "${MY_MAKE_PARAMS[@]}" )

dwb_src_prepare() {
    default

    # honor CFLAGS + nuke -Werror
    edo sed -e 's/-O[0-9]//g' -e 's/-g\( \|$\)//g' \
            -e 's/-Werror[^ ]*//g' \
            -i config.mk **/Makefile

    # actually display what it's doing
    edo sed -e 's/@\$(CC)/$(CC)/' -e '/@echo "\?\$[({]CC[})]/d' \
            -i **/Makefile
}

