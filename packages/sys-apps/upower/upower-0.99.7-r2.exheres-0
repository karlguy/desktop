# Copyright 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service udev-rules option-renames [ renames=[ 'systemd providers:systemd' ] ]

SUMMARY="An abstraction for enumerating power devices"
HOMEPAGE="https://${PN}.freedesktop.org"
DOWNLOADS="${HOMEPAGE}/releases/${PNV}.tar.xz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    idevice [[ description = [ Enable support for iPod, iPad, and iPhone battery status ] ]]
    ( linguas: fr it pl sv )
    ( providers: elogind pm-utils systemd ) [[
        *description = [ Suspend/Resume provider ]
        number-selected = at-most-one
    ]]
"

# uses system dbus socket
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-libs/libxslt
        dev-util/intltool[>=0.40.0]
        sys-devel/gettext[>=0.17]
        virtual/pkg-config[>=0.21]
        gobject-introspection? ( gnome-desktop/gobject-introspection[>=0.9.9] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.11] )
    build+run:
        dev-libs/glib:2[>=2.34.0]
        gnome-desktop/libgudev
        virtual/usb:1
        idevice? (
            app-pda/libimobiledevice[>=0.9.7]
            dev-libs/libplist[>=0.12]
        )
    run:
        providers:elogind? ( sys-auth/elogind )
        providers:pm-utils? ( sys-power/pm-utils[>=1.4.1] )
        providers:systemd? ( sys-apps/systemd )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --localstatedir=/var
    --disable-static
    --with-backend=linux
    --with-systemdsystemunitdir=${SYSTEMDSYSTEMUNITDIR}
    --with-udevrulesdir=${UDEVRULESDIR}
    # Probably needed until KDE (solid) has been ported
    # http://cgit.freedesktop.org/upower/commit/?id=372c2f8d2922add987683a24b5d69902e05e2f97
    # http://lists.freedesktop.org/archives/devkit-devel/2013-January/001339.html
    --enable-deprecated
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'gobject-introspection introspection' gtk-doc )
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    idevice
)

src_install() {
    default

    # history directory is created at runtime if missing
    edo rm -r "${IMAGE}"/var
}

